# ComputerCraft Video Converter

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Hp7VgG7w2iU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Requirements

- DotNet Core SDK (3.1) / Visual Studio
- A functioning keyboard

## Usage

1. Fill the `frames` folder with black & white images
2. Follow the steps below

   **For Visual Studio:**

   (I don't use VS myself, so this part might be wrong)

   Open the project folder in Visual Studio, and then build & run the application. In the console, press any key (except `N`) to convert the files.

   **For DotNet CLI:**

   `cd` into the repository and run `dotnet run`. After the compilation is complete, press any key (except `N`) to convert the files

3. Move the output folder (which has `player.lua` and `VIDEO` directory with multiple .txt files in it) into the ComputerCraft save file. Pastebin or `wget` might not work because of the size limitations.
   The folder you're looking for is located at `.minecraft/saves/<world name>/computercraft/{disk/computer}/<Disk or Computer ID>`. Disks are recommended
4. In the game, locate `player.lua` and run it.

- If you put the files anywhere that's not the root directory, you might get an error saying that `/VIDEO`, open the script and replace every `/VIDEO` with the path that goes to `player.lua`
  e.g: If `player.lua` is in `/cool/folder/`, replace all `/VIDEO` with `/cool/folder/VIDEO`

5. If you did everything right, the video should play. Sit back, and enjoy your 18p20fps video playback
