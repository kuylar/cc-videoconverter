﻿using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;

namespace bad_apple_frames
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Will load frames from frames/*.jpg. Continue? [Y/n]");
			switch (Console.ReadKey().KeyChar.ToString().ToLower())
			{
				case "n":
					Environment.Exit(0);
					break;
			}

			string[] frames = Directory.GetFiles("./frames");
			Array.Sort(frames, StringComparer.InvariantCulture);
			Directory.CreateDirectory("output");
			Directory.CreateDirectory("output/VIDEO");
			foreach (string file in frames)
			{
				if (file.Contains("README")) continue;
				Bitmap img = new Bitmap(file);
				byte[] fileBytes = new byte[img.Width * img.Height + img.Height];
				string fileText = "";

				for (int j = 0; j < img.Height; j++)
				{
					fileText += '-';
					for (int i = 0; i < img.Width; i++)
					{
						Color pixel = img.GetPixel(i, j);
						int index = j * img.Width + i;
						
						if (pixel.GetBrightness() > 0.6)
							fileText += '#';
						else if (pixel.GetBrightness() > 0.2)
							fileText += 'O';
						else
							fileText += ' ';
					}
					fileText += '\n';
				}

				string[] fileNameParts = file.Split("/")[2].Split(".");
				File.WriteAllText("output/" + fileNameParts[fileNameParts.Length - 2] + ".txt", fileText);
				Console.WriteLine(file + " complete!");
			}
		}
	}
}
