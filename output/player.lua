frames = fs.list("/VIDEO")

frame = 0
total = table.getn(frames)

while frame < total do
	frame = frame + 1
	term.clear()
	term.setCursorPos(1, 1)
	for line in io.lines("/VIDEO/" .. frames[frame]) do 
		write(line)
	end
	write("Frame " .. frame .. " / " .. total)
	sleep(0.01) -- i think computercraft defaults to 20fps, but this works so ill leave it like that
end
term.setCursorPos(1, 1)
term.clear()
